package constraints;

import jsonvalidator.retrieved.CompoundTable;
import jsonvalidator.retrieved.Party;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.util.ESat;

import java.util.ArrayList;

import static helper.ArrayHelper.arraysContainsCommonValues;

public class LongDeadZonesConstraint extends Propagator<IntVar> {

    private static final int MINIMUM_TIME_INTERVAL = 2;
    private int dinnerDuration;
    private ArrayList<CompoundTable> compoundTables;
    private ArrayList<Party> parties;


    public LongDeadZonesConstraint(
            int dinnerDuration,
            ArrayList<CompoundTable> compoundTables,
            ArrayList<Party> parties,
            IntVar... vars
    ) {
        super(vars);
        this.dinnerDuration = dinnerDuration;
        this.compoundTables = compoundTables;
        this.parties = parties;
    }


    @Override
    public void propagate(int i) throws ContradictionException {

            for(Party p1 : parties) {

                for(Party p2 : parties) {

                    if(p1 != p2) {

                        int p1EndTime = p1.getStart() + dinnerDuration;
                        int timeGap = p2.getStart() - p1EndTime;
                        if(timeGap > MINIMUM_TIME_INTERVAL && timeGap < dinnerDuration) {
                            for(CompoundTable ct1 : compoundTables) {

                                if (p1.getSize() <= ct1.getCapacity()) {

                                    int[] tables1 = ct1.getTables();
                                    IntVar p2cTables = vars[p2.getvIndex()];

                                    for (CompoundTable ct2 : compoundTables) {
                                        if (p2cTables.contains(ct2.getId())) {

                                            int[] tables2 = ct2.getTables();
                                            if (arraysContainsCommonValues(tables1, tables2)) {

                                                if (vars[p1.getvIndex()].isInstantiatedTo(ct1.getId()) &&
                                                        vars[p2.getvIndex()].isInstantiatedTo(ct2.getId())
                                                )
                                                    fails();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
    }

    @Override
    public ESat isEntailed() {
        return ESat.UNDEFINED;
    }
}
