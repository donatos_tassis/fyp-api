package constraints;

import jsonvalidator.retrieved.CompoundTable;
import jsonvalidator.retrieved.Party;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.util.ESat;

import java.util.ArrayList;


public class CapacityConstraint extends Propagator<IntVar> {

    private ArrayList<ArrayList<Party>> dominantSets;
    private ArrayList<CompoundTable> compoundTables;
    private int totalTables;


    public CapacityConstraint(
            ArrayList<ArrayList<Party>> dominantSets,
            ArrayList<CompoundTable> compoundTables,
            int totalTables,
            IntVar... vars
    ) {
        super(vars);
        this.dominantSets = dominantSets;
        this.compoundTables = compoundTables;
        this.totalTables = totalTables;
    }


    @Override
    public void propagate(int i) throws ContradictionException {

        for (ArrayList<Party> listPPS : dominantSets) {
            int usableTables = totalTables;

            for(Party party : listPPS) {

                    for(CompoundTable ct : compoundTables) {
                        if(vars[party.getvIndex()].isInstantiatedTo(ct.getId())) {
                            usableTables -= ct.getTables().length;
                            break;
                        }
                    }
            }

            if(usableTables < 0)
                fails();
        }
    }

    @Override
    public ESat isEntailed() {
        return ESat.UNDEFINED;
    }
}
