package constraints;

import jsonvalidator.retrieved.CompoundTable;
import jsonvalidator.retrieved.Party;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.util.ESat;

import java.util.ArrayList;


public class OverSizedTableConstraint extends Propagator<IntVar> {

    private ArrayList<CompoundTable> compoundTables;
    private ArrayList<Party> parties;


    public OverSizedTableConstraint(
            ArrayList<CompoundTable> compoundTables,
            ArrayList<Party> parties,
            IntVar... vars
    ) {
        super(vars);
        this.compoundTables = compoundTables;
        this.parties = parties;
    }


    @Override
    public void propagate(int i) throws ContradictionException {

        for(Party party : parties) {
            for(CompoundTable compoundTable : compoundTables) {
                if(party.getSize() < 4 && compoundTable.getCapacity() > 4) {
                    vars[party.getvIndex()].removeValue(compoundTable.getId(), this);
                }
                int emptySeats = compoundTable.getCapacity() - party.getSize();
                if(party.getSize() >= 4 && emptySeats > 0.35*compoundTable.getCapacity()) {
                    vars[party.getvIndex()].removeValue(compoundTable.getId(), this);
                }
            }
        }
    }

    @Override
    public ESat isEntailed() {
        return ESat.UNDEFINED;
    }
}
