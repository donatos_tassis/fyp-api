package constraints;

import jsonvalidator.retrieved.CompoundTable;
import jsonvalidator.retrieved.Party;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.util.ESat;

import java.util.ArrayList;

import static helper.ArrayHelper.*;

public class OccupancyConstraint extends Propagator<IntVar> {

    private ArrayList<ArrayList<Party>> dominantSets;
    private ArrayList<CompoundTable> compoundTables;


    public OccupancyConstraint(
            ArrayList<ArrayList<Party>> dominantSets,
            ArrayList<CompoundTable> compoundTables,
            IntVar... vars
    ) {
        super(vars);
        this.dominantSets = dominantSets;
        this.compoundTables = compoundTables;
    }


    @Override
    public void propagate(int i) throws ContradictionException {

        for (ArrayList<Party> listPPS : dominantSets) {

            for(Party p1 : listPPS) {

                for(Party p2 : listPPS) {

                    if(p1 != p2) {

                        for(CompoundTable ct1 : compoundTables) {

                            if (p1.getSize() <= ct1.getCapacity()) {

                                int[] tables1 = ct1.getTables();
                                IntVar p2cTables = vars[p2.getvIndex()];

                                for (CompoundTable ct2 : compoundTables) {
                                    if (ct2 != ct1 && p2cTables.contains(ct2.getId())) {

                                        int[] tables2 = ct2.getTables();
                                        if (arraysContainsCommonValues(tables1, tables2)) {

                                            if(vars[p1.getvIndex()].isInstantiatedTo(ct1.getId()) &&
                                                vars[p2.getvIndex()].isInstantiatedTo(ct2.getId())
                                            )
                                                fails();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public ESat isEntailed() {
        return ESat.UNDEFINED;
    }
}
