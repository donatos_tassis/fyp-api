package main;

import com.fasterxml.jackson.databind.ObjectMapper;
import jsonvalidator.retrieved.DataRetriever;
import models.AtomicTablesModel;
import models.CombinedTablesModel;
import models.IModel;
import models.StrictCombinedTablesModel;
import spark.Request;
import spark.Response;


import java.io.IOException;

import static spark.Spark.*;


public class Main {

    private static final double PERCENTAGE_OF_TARGET = 0.8;

    public static void main(String[] args) {
        port(getHerokuAssignedPort());
        new Routes(); // instantiate endpoints
    }

    private static IModel modelSelector(Request request) throws IOException {

        IModel model = null;
        String path = request.uri();
        switch (path)
        {
            case "/search-one":
                model = new AtomicTablesModel();
                break;
            case "/search-simple":
                model = new CombinedTablesModel();
                break;
            case "/search-strict":
                model = new StrictCombinedTablesModel();
                break;
            case "/search-hybrid":
                ObjectMapper mapper = new ObjectMapper();
                DataRetriever data = mapper.readValue(request.body(), DataRetriever.class);

                int targetCovers = data.getTargetCovers();
                int currentCovers = data.getCurrentCovers();

                if(currentCovers < PERCENTAGE_OF_TARGET * targetCovers)
                    model = new StrictCombinedTablesModel();
                else
                    model = new CombinedTablesModel();
                break;
        }

        return model;
    }

    static String execute(Request request, Response response) throws IOException {
        IModel model = modelSelector(request);

        return model.execute(request, response);
    }

    private static int getHerokuAssignedPort() {
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (processBuilder.environment().get("PORT") != null)
            return Integer.parseInt(processBuilder.environment().get("PORT"));

        return 4567; //return default port if aws-port isn't set (i.e. on localhost)
    }
}
