package main;

import static spark.Spark.get;
import static spark.Spark.post;

public class Routes {

    Routes() {
        setupEndpoints();
    }

    private void setupEndpoints() {
        get("/test", (request, response) -> "Server is running"); //testing purpose endpoint that starts the server
        post("/search-one", "application/json", Main::execute);
        post("/search-simple", "application/json", Main::execute);
        post("/search-strict", "application/json", Main::execute);
        post("/search-hybrid", "application/json", Main::execute);
    }
}
