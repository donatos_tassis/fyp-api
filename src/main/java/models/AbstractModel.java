package models;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import helper.CustomErrorException;
import jsonvalidator.retrieved.DataRetriever;
import jsonvalidator.retrieved.Party;
import jsonvalidator.retrieved.Table;
import jsonvalidator.send.DataSerializer;
import jsonvalidator.send.PartySend;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.search.limits.TimeCounter;
import org.chocosolver.solver.search.loop.monitors.IMonitorRestart;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.search.strategy.selectors.values.IntDomainMax;
import org.chocosolver.solver.search.strategy.selectors.values.IntDomainMiddle;
import org.chocosolver.solver.search.strategy.selectors.values.IntDomainMin;
import org.chocosolver.solver.search.strategy.selectors.variables.AntiFirstFail;
import org.chocosolver.solver.search.strategy.selectors.variables.Cyclic;
import org.chocosolver.solver.search.strategy.selectors.variables.FirstFail;
import org.chocosolver.solver.search.strategy.selectors.variables.InputOrder;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.Task;
import spark.Request;
import spark.Response;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;

import static helper.ArrayHelper.arrayListOfArraysToArray;
import static helper.ArrayHelper.mergeArraysOfSameIndexIntoSingleArray;
import static org.chocosolver.util.tools.MathUtils.sum;

public abstract class AbstractModel implements IModel {

    private static final int SEARCH_LIMIT = 30000; // in milliseconds
    private static final long RESTART_LIMIT = 2000000000; // in nanoseconds
    private static final double GEOMETRICAL_FACTOR = 1.02;  // the geometrical rate of increasing limit
    private static final int RESTARTS_LIMIT = 10; // limit number of restarts

    private ObjectMapper mapper;  // maps the json body request into java objects
    Model model;  // the model
    Solver solver;  // the solver
    DataRetriever data;  // data retrieved from request and transformed into object
    String error = null; // the error message

    int dinnerDuration; // the average dinner duration
    int timeSlots; // total restaurant's timeSlots
    ArrayList<ArrayList<Party>> dominantSets = new ArrayList<>(); // list of parties per dominant time slot
    ArrayList<Table> tables = new ArrayList<>(); // list of tables
    ArrayList<Party> parties = new ArrayList<>(); // list of parties
    ArrayList<Solution> solutions = new ArrayList<>(); // list of possible solutions
    ArrayList<ArrayList<Party>> partiesPerTimeSlot = new ArrayList<>(); // list of parties per time slot

    Task[] vTasks; // reservation requests as tasks
    IntVar[] vParties; // decision variables for parties
    IntVar[] vPartySizes; // party sizes
    IntVar[] vTablesPerParty; // possible values per party
    IntVar vRestaurantCapacity; // restaurants' capacity
    IntVar vTotalTables; // total single (not combined) tables of the restaurant
    IntVar vObjective; // the objective of the solution (minimum empty chairs on occupied tables)

    @Override
    public void setUp(Request request, Response response) throws IOException {
        response.type("application/json");
        mapper = new ObjectMapper();
        try {
            data = mapper.readValue(request.body(), DataRetriever.class);
            if(!data.isValid()) {
                response.status(400);

                error = new CustomErrorException().jsonError("Invalid data passed", data.getFailedRequests());
            }
        } catch (UnrecognizedPropertyException exception){
            error = new CustomErrorException().jsonError("Invalid data passed", data.getFailedRequests());
        }
    }

    @Override
    public Model getModel() {
        return model;
    }


    @Override
    public void buildData() {
        timeSlots = data.getTimeSlots();
        dinnerDuration = data.getDinnerDuration();
        tables = data.getTablesList();
        parties = data.getPartiesList();
        // array of all tables capacity
        int[] tableCapacities = mergeArraysOfSameIndexIntoSingleArray(
                arrayListOfArraysToArray(data.getTablesArray(), 1),
                1
        );

        vTasks = new Task[parties.size()];
        vPartySizes = new IntVar[parties.size()];
        vTablesPerParty = new IntVar[parties.size()];
        vParties = new IntVar[parties.size()];
        vRestaurantCapacity = model.intVar("RC", sum(tableCapacities));
        vTotalTables = model.intVar("TT", tables.size());
        vObjective = model.intVar("Obj", 0, Integer.MAX_VALUE / 2, false);
    }

    @Override
    public void configureSearch() {
        solver = model.getSolver();
        searchWithRestarts(vParties);
    }

    /**
     * Builds the tasks (Parties)
     */
    void buildTasks() {
        int pIndex = 0;
        for (Party party : parties) {
            IntVar start = model.intVar("Pstart" + party.getId(), party.getStart());
            IntVar duration = model.intVar("Pdur" + party.getId(), dinnerDuration);
            IntVar end = model.intVar("Pend" + party.getId(), party.getStart() + dinnerDuration);
            vPartySizes[pIndex] = model.intVar("Psize" + party.getId(), party.getSize());
            vTablesPerParty[pIndex] = model.intVar("TPP" + party.getId(), 1);
            vTasks[pIndex] = new Task(start, duration, end);
            pIndex++;
        }
    }

    /**
     * the execute function that needs to be called to build the model and solve it
     *
     * @param request
     * @param response
     *
     * @return Spark response in json format
     * @throws IOException
     */
    public String execute(Request request, Response response) throws IOException {
        setUp(request, response);
        if(error != null)
            return error;

        buildModel(data, response);
        if(error != null)
            return error;

        buildConstraints();
        configureSearch();

        return solve(response);
    }


    /**
     * Given the available parties it calculates and stores the dominant timeSlots
     *
     * @param parties ArrayList<Party> containing the party requests
     * @param partiesPerTimeSlot ArrayList<ArrayList<Party>> that contains the parties per available timeSlot
     */
    void buildDominantTimeSlots(ArrayList<Party> parties, ArrayList<ArrayList<Party>> partiesPerTimeSlot) {

        // Calculating the parties per time slot
        for(int t = 0; t < data.getTimeSlots(); t++) {
            ArrayList<Party> partiesInThisSlot = new ArrayList<>();
            for(Party party: parties) {
                if(party.getStart() <= t + 1 && t + 1 < party.getStart() + data.getDinnerDuration()) {
                    partiesInThisSlot.add(party);
                }
            }

            partiesPerTimeSlot.add(partiesInThisSlot);
        }

        // Find dominant sets (overlapping time Slots)
        for(int t1 = 0; t1 < data.getTimeSlots(); t1++) {
            ArrayList<Party> listT1 = partiesPerTimeSlot.get(t1);
            boolean isDominantSet = true;
            for(int t2 = 0; t2 < data.getTimeSlots(); t2++) {
                if(t1 != t2) {
                    ArrayList<Party> listT2 = partiesPerTimeSlot.get(t2);
                    boolean listsAreEqual = listT1.equals(listT2);
                    boolean isSubset = listT2.containsAll(listT1);
                    if(t2 < t1 && listsAreEqual || isSubset && !listsAreEqual) {
                        isDominantSet = false;
                        break;
                    }

                }
            }
            if(isDominantSet)
                this.dominantSets.add(listT1);
        }
    }

    /**
     * Ensures that no parties overlapping in time are seated in the same table
     *
     * @param vParties IntVar[] that holds the parties Decision Variable
     */
    void noOverLapConstraint(IntVar[] vParties)
    {
        for (ArrayList<Party> listPPS : dominantSets) {
            IntVar[] vPPS = new IntVar[listPPS.size()];

            int vPPSindex = 0;
            for(Party party : listPPS) {
                vPPS[vPPSindex] = vParties[party.getvIndex()];
                vPPSindex++;
            }

            model.allDifferent(vPPS).post();
        }
    }

    /**
     * Symmetry breaker constraint ensures that similar parties with same size and same arrival time will be
     * prioritized to decrease the search tree size
     */
    void symmetryBreakerConstraint() {
        for(Party p1 : parties) {
            for(Party p2 : parties) {
                if(p1 != p2 && p1.getSize() == p2.getSize() && p1.getStart() == p2.getStart()) {
                    if(p1.getId() < p2.getId()) {
                        model.arithm(vParties[p1.getvIndex()], "<", vParties[p2.getvIndex()]).post();
                    }
                }
            }
        }
    }

    /**
     * configures a search with geometrical restarts and changing search strategy after each restart
     *
     * @param vParties the search IntVar[] parameter
     */
    private void searchWithRestarts(IntVar[] vParties)
    {
        solver.setSearch(Search.intVarSearch(
                new FirstFail(model),
                new IntDomainMin(),
                vParties
        ));
        solver.setNoGoodRecordingFromRestarts();

//        solver.showShortStatistics();
//        solver.showSolutions();

        solver.limitTime(SEARCH_LIMIT);
        solver.setGeometricalRestart(
                5 * RESTART_LIMIT,
                GEOMETRICAL_FACTOR,
                new TimeCounter(model,5 * RESTART_LIMIT),
                RESTARTS_LIMIT);

        solver.plugMonitor(new IMonitorRestart() {
            @Override
            public void afterRestart() {
                changeStrategyAfterRestart(vParties);
            }
        });
    }

    /**
     *
     * @param response
     *
     * @return String in json response format
     * @throws IOException
     */
    String findOptimalSolution(Response response) throws IOException {
        Solution optimalSolution = new Solution(model);
        int minEmptyChairs = Integer.MAX_VALUE;
        Iterator iterator = solutions.iterator();
        while(iterator.hasNext()) {
            Solution solution = (Solution) iterator.next();
            int objective = solution.getIntVal(vObjective);
            if(objective < minEmptyChairs) {
                minEmptyChairs = objective;
                optimalSolution = solution;
            }
        }

        if(optimalSolution.exists()){
            ArrayList<PartySend> responseParties = new ArrayList<>();
            for (IntVar party : vParties) {
                String name = party.getName();
                int partyId = Integer.parseInt(name.substring(1));
                int table = optimalSolution.getIntVal(party);
                PartySend responseParty = new PartySend(partyId, table);
                responseParties.add(responseParty);
            }

            DataSerializer responseData = new DataSerializer(
                    data.getRestaurant(),
                    data.getDate(),
                    minEmptyChairs,
                    data.getFailedRequests(),
                    responseParties
            );
            StringWriter sw = new StringWriter();
            mapper.writeValue(sw, responseData);
            response.status(200);

            return sw.toString();
        }
        response.status(404);
        int failedRequests = data.getFailedRequests() + 1;

        return new CustomErrorException().jsonError("No solution found ...",failedRequests);
    }

    /**
     * Defines which search strategy is going to be used after each restart
     * by counting the number of restarts
     *
     * @param vParties the search IntVar[] parameter
     */
    private void changeStrategyAfterRestart(IntVar[] vParties) {
        long restartCount = solver.getMeasures().getRestartCount();
        switch ((int)restartCount) {
            case 1:
                solver.setSearch(Search.intVarSearch(
                        new FirstFail(model),
                        new IntDomainMiddle(true),
                        vParties
                ));
                break;
            case 2:
                solver.setSearch(Search.intVarSearch(
                        new FirstFail(model),
                        new IntDomainMax(),
                        vParties
                ));
                break;
            case 3:
                solver.setSearch(Search.intVarSearch(
                        new AntiFirstFail(model),
                        new IntDomainMin(),
                        vParties
                ));
                break;
            case 4:
                solver.setSearch(Search.intVarSearch(
                        new AntiFirstFail(model),
                        new IntDomainMiddle(true),
                        vParties
                ));
                break;
            case 5:
                solver.setSearch(Search.intVarSearch(
                        new AntiFirstFail(model),
                        new IntDomainMax(),
                        vParties
                ));
                break;
            case 6:
                solver.setSearch(Search.intVarSearch(
                        new InputOrder<>(model),
                        new IntDomainMin(),
                        vParties
                ));
                break;
            case 7:
                solver.setSearch(Search.intVarSearch(
                        new InputOrder<>(model),
                        new IntDomainMiddle(true),
                        vParties
                ));
                break;
            case 8:
                solver.setSearch(Search.intVarSearch(
                        new InputOrder<>(model),
                        new IntDomainMax(),
                        vParties
                ));
                break;
            case 9:
                solver.setSearch(Search.intVarSearch(
                        new Cyclic<>(),
                        new IntDomainMin(),
                        vParties
                ));
                break;
            case 10:
                solver.setSearch(Search.intVarSearch(
                        new Cyclic<>(),
                        new IntDomainMiddle(true),
                        vParties
                ));
                break;
            default:
                solver.setSearch(Search.intVarSearch(
                        new Cyclic<>(),
                        new IntDomainMax(),
                        vParties
                ));
                break;
        }
    }
}
