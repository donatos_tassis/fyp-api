package models;

import helper.CustomErrorException;
import jsonvalidator.retrieved.DataRetriever;
import jsonvalidator.retrieved.Party;
import jsonvalidator.retrieved.Table;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.search.loop.monitors.IMonitorSolution;
import org.chocosolver.solver.variables.IntVar;
import spark.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

import static helper.ArrayHelper.*;


public class AtomicTablesModel extends AbstractModel {

    @Override
    public void buildModel(DataRetriever data, Response response) throws IOException {
        model = new Model("Find table problem - S-01");

        buildData();
        buildTasks();
        setInitialDomains(response);
        buildDominantTimeSlots(parties, partiesPerTimeSlot);
    }

    @Override
    public void buildConstraints() {
        noOverLapConstraint(vParties);
        // ensures that at any given time the total dinners dont expand the restaurant's capacity
        model.cumulative(vTasks, vPartySizes, vRestaurantCapacity).post();
        // ensures that at any given time the total parties dont expand the restaurant's total tables
        model.cumulative(vTasks, vTablesPerParty, vTotalTables).post();
        // ensures that at any given time the total parties of size Ps dont expand the the restaurant's
        // total tables of at least capacity Tc >= Ps
        tablesPerPartySizeConstraint();
        symmetryBreakerConstraint();
    }

    @Override
    public void buildData() {
        super.buildData();
    }

    private void setInitialDomains(Response response) throws IOException {
        int index = 0;
        for(Party party : parties) {
            ArrayList<Integer> tablesList = new ArrayList<>();
            for(Table table : tables) {
                if(party.getSize() <= table.getCapacity())
                    tablesList.add(table.getId());
            }

            if(tablesList.size() != 0) {
                party.setvIndex(index);
                vParties[index] = model.intVar("P" + party.getId(), arrayListToIntArray(tablesList));
                index++;
            } else {
                response.status(400);
                int failedRequests = data.getFailedRequests() + 1;

                error =  new CustomErrorException().jsonError("Overcrowded party!!", failedRequests);
            }
        }
    }

    private void tablesPerPartySizeConstraint() {
        for(int size = data.getMinPartySize(); size <= data.getMaxPartySize(); size++) {
            int maxTPS = 0;  // max tables per size
            for(Table table : tables) {
                if(table.getCapacity() >= size)
                    ++maxTPS;
            }

            for(int t = 0; t < timeSlots; t++) {
                int maxPPS = 0;  // total number of parties per size
                ArrayList<Party> parties = partiesPerTimeSlot.get(t);
                for(Party party : parties) {
                    if(party.getSize() >= size) {
                        maxPPS++;
                    }
                }

                model.arithm(model.intVar(maxPPS), "<=", model.intVar(maxTPS)).post();
            }
        }
    }

    @Override
    public String solve(Response response) throws IOException {
        solver.plugMonitor((IMonitorSolution) () -> {
            Solution solution = new Solution(model);
            solution.record();
            int objective = 0;
            for (IntVar party : vParties) {
                int tableId = solution.getIntVal(party);
                Optional<Table> tbl = tables.stream().filter(
                        t -> t.getId() == tableId
                ).findFirst();
                Table tableObj = tbl.get();

                int partyId = Integer.parseInt(party.getName().substring(1));
                Optional<Party> pt = parties.stream().filter(
                        p -> p.getId() == partyId
                ).findFirst();
                Party partyObj = pt.get();
                objective += tableObj.getCapacity() - partyObj.getSize();
            }

            solution.setIntVal(vObjective, objective);
            solutions.add(solution);

            try {
                solution.restore();
            } catch (ContradictionException e) {
                e.printStackTrace();
            }
        });

        while(solver.solve());

        return findOptimalSolution(response);
    }
}
