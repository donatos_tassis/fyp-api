package models;

import constraints.CapacityConstraint;
import constraints.OccupancyConstraint;
import helper.CustomErrorException;
import jsonvalidator.retrieved.CompoundTable;
import jsonvalidator.retrieved.DataRetriever;
import jsonvalidator.retrieved.Party;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.constraints.Constraint;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.search.loop.monitors.IMonitorSolution;
import org.chocosolver.solver.variables.IntVar;
import spark.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;


import static helper.ArrayHelper.*;


public class CombinedTablesModel extends AbstractModel {

    ArrayList<CompoundTable> compoundTables; // list of all combinations of tables that can be grouped together

    @Override
    public void buildModel(DataRetriever data, Response response) throws IOException {
        model = new Model("Find table problem - S-02");

        buildData();
        buildTasks();
        setInitialDomains(response);
        buildDominantTimeSlots(parties, partiesPerTimeSlot);
    }


    @Override
    public void buildConstraints() {
        noOverLapConstraint(vParties);
        new Constraint("OcCst", new OccupancyConstraint(dominantSets, compoundTables, vParties)).post();

        // ensures that at any given time the total dinners dont expand the restaurant's capacity
        model.cumulative(vTasks, vPartySizes, vRestaurantCapacity).post();
        // ensures that at any given time the total parties dont expand the restaurant's total tables
        model.cumulative(vTasks, vTablesPerParty, vTotalTables).post();
        // ensures that at any given time the total parties of size Ps dont expand the the restaurant's
        // total compound tables of at least capacity Tc >= Ps
        compoundTablesPerPartySizeConstraint();
        symmetryBreakerConstraint();

        new Constraint(
                "capCst",
                new CapacityConstraint(dominantSets, compoundTables, tables.size(), vParties)
        ).post();
    }

    @Override
    public void buildData() {
        super.buildData();
        compoundTables = data.getCompoundTables();
    }

    private void setInitialDomains(Response response) throws IOException {
        int pIndex = 0;
        for(Party party : parties) {
            ArrayList<Integer> compoundTablesList = new ArrayList<>();
            for(CompoundTable compoundTable: compoundTables) {
                if(party.getSize() <= compoundTable.getCapacity())
                    compoundTablesList.add(compoundTable.getId());
            }

            if(compoundTablesList.size() > 0) {
                party.setvIndex(pIndex);
                vParties[pIndex] = model.intVar("P" + party.getId(), arrayListToIntArray(compoundTablesList));
                pIndex++;
            } else {
                response.status(400);
                int failedRequests = data.getFailedRequests() + 1;

                error =  new CustomErrorException().jsonError("Overcrowded party!!", failedRequests);
            }
        }
    }

    private void compoundTablesPerPartySizeConstraint() {
        for(int size = data.getMinPartySize(); size <= data.getMaxPartySize(); size++) {
            int maxCTPS = 0;  // max compound tables per size
            for(CompoundTable compoundTable : compoundTables) {
                if(compoundTable.getCapacity() >= size)
                    ++maxCTPS;
            }

            for(int t = 0; t < timeSlots; t++) {
                int maxPPS = 0;  // total number of parties per size
                ArrayList<Party> parties = partiesPerTimeSlot.get(t);
                for(Party party : parties) {
                    if(party.getSize() >= size) {
                        maxPPS++;
                    }
                }

                model.arithm(model.intVar(maxPPS), "<=", model.intVar(maxCTPS)).post();
            }
        }
    }

    @Override
    public String solve(Response response) throws IOException {
        solver.plugMonitor((IMonitorSolution) () -> {
            Solution solution = new Solution(model);
            solution.record();
            int objective = 0;
            for (IntVar party : vParties) {
                int compoundTableId = solution.getIntVal(party);
                Optional<CompoundTable> ctbl = compoundTables.stream().filter(
                        ct -> ct.getId() == compoundTableId
                ).findFirst();
                CompoundTable compoundTableObj = ctbl.get();

                int partyId = Integer.parseInt(party.getName().substring(1));
                Optional<Party> pt = parties.stream().filter(
                        p -> p.getId() == partyId
                ).findFirst();
                Party partyObj = pt.get();
                objective += compoundTableObj.getCapacity() - partyObj.getSize();
            }

            solution.setIntVal(vObjective, objective);
            solutions.add(solution);

            try {
                solution.restore();
            } catch (ContradictionException e) {
                e.printStackTrace();
            }
        });

        while(solver.solve());

        return findOptimalSolution(response);
    }
}
