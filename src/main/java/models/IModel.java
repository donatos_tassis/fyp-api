package models;

import jsonvalidator.retrieved.DataRetriever;
import org.chocosolver.solver.Model;
import spark.Request;
import spark.Response;

import java.io.IOException;

public interface IModel {

    void setUp(Request request, Response response)throws IOException;

    void buildModel(DataRetriever data, Response response) throws IOException;

    void buildData();

    void buildConstraints();

    Model getModel();

    void configureSearch();

    String solve(Response response) throws IOException;

    String execute(Request request, Response response)throws IOException;
}
