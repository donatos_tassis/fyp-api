package models;

import constraints.LongDeadZonesConstraint;
import constraints.OverSizedTableConstraint;
import org.chocosolver.solver.constraints.Constraint;

public class StrictCombinedTablesModel extends CombinedTablesModel {

    @Override
    public void buildConstraints() {
        super.buildConstraints();
        new Constraint(
                "deadZonesCst",
                new LongDeadZonesConstraint(dinnerDuration, compoundTables, parties, vParties)
        ).post();

        new Constraint(
                "overSizedCst",
                new OverSizedTableConstraint(compoundTables, parties, vParties)
        ).post();
    }
}
