package jsonvalidator.send;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jsonvalidator.Validable;

import java.util.ArrayList;

@JsonIgnoreProperties({"valid"})
public class DataSerializer implements Validable {

    // the restaurant id
    private int restaurant;
    // the date in YYYY-MM-DD format
    private String date;
    // objective - empty chairs at occupied tables or group of tables
    private int emptyChairs;
    // failed requests of the day
    private int failedRequests;
    // list of parties (reservation requests)
    private ArrayList<PartySend> parties = new ArrayList<>();

    /**
     * Constructor of DataSerializer
     *
     * @param restaurant the restaurant id
     * @param date the date in YYYY-MM-DD format
     * @param emptyChairs the total empty chairs at occupied tables or group of tables
     * @param parties the parties data
     */
    public DataSerializer(int restaurant, String date, int emptyChairs, int failedRequests, ArrayList<PartySend> parties) {
        this.restaurant = restaurant;
        this.date = date;
        this.emptyChairs = emptyChairs;
        this.failedRequests = failedRequests;
        this.parties = parties;
    }

    public int getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(int restaurant) {
        this.restaurant = restaurant;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<PartySend> getParties() {
        return parties;
    }

    public void setParties(ArrayList<PartySend> parties) {
        this.parties = parties;
    }

    public int getEmptyChairs() {
        return emptyChairs;
    }

    public int getFailedRequests() {
        return failedRequests;
    }

    public void setFailedRequests(int failedRequests) {
        this.failedRequests = failedRequests;
    }

    public void setEmptyChairs(int emptyChairs) {
        this.emptyChairs = emptyChairs;
    }

    @Override
    public boolean isValid() {
        return !parties.isEmpty();
    }
}
