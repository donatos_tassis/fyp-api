package jsonvalidator.send;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jsonvalidator.Validable;

@JsonIgnoreProperties({"valid"})
public class PartySend implements Validable {
    // the reservation request id (party_id)
    private int id;
    // the table id assigned to the party
    private int table;

    public PartySend(int id, int table) {
        this.id = id;
        this.table = table;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTable() {
        return table;
    }

    public void setTable(int table) {
        this.table = table;
    }

    @Override
    public boolean isValid() {
        return id > 0 && table > 0;
    }
}
