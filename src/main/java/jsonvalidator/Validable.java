package jsonvalidator;

public interface Validable {

    /**
     * Used to validate the json format
     * @return boolean
     */
    boolean isValid();
}
