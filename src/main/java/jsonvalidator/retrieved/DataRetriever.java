package jsonvalidator.retrieved;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jsonvalidator.Validable;

import java.util.ArrayList;
import java.util.Collections;

@JsonIgnoreProperties({"valid","tablesArray","currentCovers","partiesArray","maxTableCapacity","partiesList","tablesList","maxPartySize","minPartySize"})
public class DataRetriever implements Validable {

    private int restaurant;
    private String date;
    private int timeSlots;
    private int targetCovers;
    private int failedRequests;
    private int dinnerDuration;
    private ArrayList<Table> tables = new ArrayList<>();
    private ArrayList<Party> parties = new ArrayList<>();
    private ArrayList<CompoundTable> compoundTables = new ArrayList<>();


    public int getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(int restaurant) {
        this.restaurant = restaurant;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getTimeSlots() {
        return timeSlots;
    }

    public void setTimeSlots(int timeSlots) {
        this.timeSlots = timeSlots;
    }

    public int getTargetCovers() {
        return targetCovers;
    }

    public void setTargetCovers(int targetCovers) {
        this.targetCovers = targetCovers;
    }

    public int getFailedRequests() {
        return failedRequests;
    }

    public void setFailedRequests(int failedRequests) {
        this.failedRequests = failedRequests;
    }

    public int getDinnerDuration() {
        return dinnerDuration;
    }

    public void setDinnerDuration(int dinnerDuration) {
        this.dinnerDuration = dinnerDuration;
    }

    public ArrayList<Table> getTables() {
        return tables;
    }

    public ArrayList<int[]> getTablesArray() {
        ArrayList<int[]> tables = new ArrayList<>();

        for (Table tableObj : this.tables) {
            int[] table = new int[2];
            table[0] = tableObj.getId();
            table[1] = tableObj.getCapacity();

            tables.add(table);
        }

        return tables;
    }

    public ArrayList<Table> getTablesList() {
        Collections.sort(tables);

        return tables;
    }

    public void setTables(ArrayList<Table> tables) {
        this.tables = tables;
    }

    public ArrayList<Party> getParties() {
        return parties;
    }

    public ArrayList<int[]> getPartiesArray() {
        ArrayList<int[]> parties = new ArrayList<>();

        for (Party partyObj : this.parties) {
            int[] party = new int[3];
            party[0] = partyObj.getId();
            party[1] = partyObj.getSize();
            party[2] = partyObj.getStart();
            parties.add(party);
        }

        return parties;
    }

    public ArrayList<Party> getPartiesList() {
        Collections.sort(parties);

        return parties;
    }

    public void addParty(Party party) {
        this.parties.add(party);
    }

    public void removeLastParty() {
        Party lastParty = this.getParties().get(parties.size() - 1);
        this.parties.remove(lastParty);
    }

    public void setParties(ArrayList<Party> parties) {
        this.parties = parties;
    }

    public ArrayList<CompoundTable> getCompoundTables() {
        return compoundTables;
    }

    public void setCompoundTables(ArrayList<CompoundTable> compoundTables) {
        this.compoundTables = compoundTables;
    }

    public int getMinPartySize() {
        int min = 100;

        for(Party party : parties) {
            if(party.getSize() < min)
                min = party.getSize();
        }

        return min;
    }

    public int getMaxPartySize() {
        int max = 1;

        for(Party party : parties) {
            if(party.getSize() > max)
                max = party.getSize();
        }

        return max;
    }

    public int getCurrentCovers() {
        int covers = 0;

        for(Party party : parties) {
            covers += party.getSize();
        }

        return covers;
    }

    public int getMaxTableCapacity() {
        int max = 1;

        for(CompoundTable ct : compoundTables) {
            if(ct.getCapacity() > max)
                max = ct.getCapacity();
        }

        return max;
    }

    @Override
    public boolean isValid() {
        for(Party party : getPartiesList()) {
            if(!party.isValid())
                return false;
        }

        for(Table table : getTablesList()) {
            if(!table.isValid())
                return false;
        }

        return timeSlots > 0 && dinnerDuration > 0 && targetCovers > 0 && failedRequests >= 0 && !tables.isEmpty() && !parties.isEmpty();
    }
}
