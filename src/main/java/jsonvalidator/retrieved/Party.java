package jsonvalidator.retrieved;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jsonvalidator.Validable;
import org.jetbrains.annotations.NotNull;

@JsonIgnoreProperties({"valid"})
public class Party implements Validable, Comparable {

    private int id;
    private int size;
    private int start;
    private int compoundTable = 0;
    @JsonIgnore
    private int vIndex = -1;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getCompoundTable() {
        return compoundTable;
    }

    public void setCompoundTable(int compoundTable) {
        this.compoundTable = compoundTable;
    }

    public int getvIndex() {
        return vIndex;
    }

    public void setvIndex(int vIndex) {
        this.vIndex = vIndex;
    }

    @Override
    public boolean isValid() {
        return id > 0 && size > 0 && start > 0;
    }

    @Override
    public int compareTo(@NotNull Object party) {
        int compareId = ((Party) party).getId();

        return this.id-compareId;
    }
}
