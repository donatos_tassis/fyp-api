package jsonvalidator.retrieved;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jsonvalidator.Validable;
import org.jetbrains.annotations.NotNull;

@JsonIgnoreProperties({"valid"})
public class Table implements Validable, Comparable {

    private int id;
    private int capacity;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public boolean isValid() {
        return id > 0 && capacity > 0;
    }

    @Override
    public int compareTo(@NotNull Object table) {
        int compareId = ((Table) table).getId();

        return this.id-compareId;
    }
}
