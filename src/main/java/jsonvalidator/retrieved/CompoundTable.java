package jsonvalidator.retrieved;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jsonvalidator.Validable;

@JsonIgnoreProperties({"valid"})
public class CompoundTable implements Validable {

    private int id;
    private int[] tables;
    private int capacity;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int[] getTables() {
        return tables;
    }

    public void setTables(int[] tables) {
        this.tables = tables;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public boolean isValid() {
        return id > 0 && capacity > 0 && tables.length > 0;
    }
}
