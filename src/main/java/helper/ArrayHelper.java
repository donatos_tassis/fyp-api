package helper;

import org.jetbrains.annotations.NotNull;

import java.util.*;


public class ArrayHelper {


    /**
     * @param arrayList the ArrayList<Integer> to be converted to array
     *
     * @return int[]
     */
    public static int[] arrayListToIntArray(@NotNull ArrayList<Integer> arrayList)
    {
        int[] array = new int[arrayList.size()];
        Iterator iterator = arrayList.iterator();

        for (int i = 0; i < array.length; i++) {
            array[i] = (int) iterator.next();
        }

        return array;
    }

    /**
     * @param arrayList the ArrayList<int[]> to be converted to array
     * @param innerArraySize the size of the inner array
     *
     * @return int[][]
     */
    public static int[][] arrayListOfArraysToArray(@NotNull ArrayList<int[]> arrayList, int innerArraySize)
    {
        int[][] array = new int[arrayList.size()][innerArraySize];
        Iterator iterator = arrayList.iterator();

        for (int i = 0; i < array.length; i++) {
            array[i] = (int[]) iterator.next();
        }

        return array;
    }

    /**
     * @param array the array of integers to be converted to List<Integer>
     *
     * @return List
     */
    public static List<Integer> intArrayToList(@NotNull int[] array)
    {
        List<Integer> list = new ArrayList<>();

        for (int i : array) {
            list.add(i);
        }

        return list;
    }

    /**
     * Given an array int[][] it picks a single index from the inner array from every outer array
     * and merge them into a single int[] array
     * @param array the array[][] to cherry pick the jsonvalidator from
     * @param index the index of the inner array to cherry pick the jsonvalidator from
     *
     * @return int[]
     */
    public static int[] mergeArraysOfSameIndexIntoSingleArray(int[][] array, int index)
    {
        int[] mergedArray = new int[array.length];

        for(int i = 0; i < array.length; i++) {
            mergedArray[i] = array[i][index];
        }

        return mergedArray;
    }

    /**
     * Compares two arrays of integers and if at least a common value is found among them
     * it returns true, otherwise it returns false
     *
     * @param array1 the 1st array to compare
     * @param array2 the 2nd array to compare
     *
     * @return boolean
     */
    public static boolean arraysContainsCommonValues(int[] array1, int[] array2)
    {
        for(int i : array1) {
            for(int j : array2) {
                if (i == j)
                    return true;
            }
        }

        return false;
    }

    /**
     * Compares two List of integers and if at least a common value is found among them
     * it returns true, otherwise it returns false
     *
     * @param list1 the 1st List to compare
     * @param list2 the 2nd List to compare
     *
     * @return boolean
     */
    public static boolean listContainsCommonValues(List<Integer> list1, List<Integer> list2)
    {
        for(int i : list1) {
            for(int j : list2) {
                if (i == j)
                    return true;
            }
        }

        return false;
    }
}
