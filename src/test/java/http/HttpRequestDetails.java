package http;


import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class HttpRequestDetails {
    public String body;
    private Map<String, String> headers = new HashMap();

    public HttpRequestDetails() {
    }

    public Map<String, String> getHeaders() {
        return this.headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers.putAll(headers);
    }

    public void addHeader(String key, String value) {
        this.headers.put(key, value);
    }

    public String raw() {
        StringBuilder rawOutput = new StringBuilder();
        Iterator var2 = this.headers.keySet().iterator();

        String key;
        while(var2.hasNext()) {
            key = (String)var2.next();
            if (this.headers.get(key) == null) {
                rawOutput.append(key);
                rawOutput.append("\n");
            }
        }

        var2 = this.headers.keySet().iterator();

        while(var2.hasNext()) {
            key = (String)var2.next();
            if (this.headers.get(key) != null) {
                rawOutput.append(key);
                rawOutput.append(": ");
                rawOutput.append((String)this.headers.get(key));
                rawOutput.append("\n");
            }
        }

        if (this.body != null) {
            rawOutput.append("\n");
            rawOutput.append(this.body);
        }

        return rawOutput.toString();
    }
}

