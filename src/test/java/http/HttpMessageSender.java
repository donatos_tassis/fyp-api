package http;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class HttpMessageSender {

    private HttpRequestSender sender;
    private URL baseUrl;
    public String DEFAULT_USER_AGENT = "Mozilla/5.0";
    public final String HEADER_USER_AGENT = "User-Agent";
    public final String HEADER_CONTENT_TYPE = "Content-Type";
    public final String HEADER_ACCEPT = "Accept";
    public final String HEADER_AUTHORIZATION = "Authorization";
    public final String CONTENT_XML = "application/xml";
    public final String CONTENT_JSON = "application/json";
    public Map<String, String> headers = new HashMap();
    private String proxyHost;
    private int proxyPort;

    public HttpMessageSender(String baseUrl) {
        try {
            this.baseUrl = new URL(baseUrl);
        } catch (MalformedURLException var3) {
            var3.printStackTrace();
            System.out.println("*** BASE URL is wrong!! " + baseUrl);
        }

        this.headers = new HashMap();
        this.sender = new HttpRequestSender((String)null, 0);
    }

    public HttpRequestDetails getLastRequest() {
        return this.sender.lastRequest;
    }

    public void setProxy(String ip, int port) {
        this.proxyHost = ip;
        this.proxyPort = port;
        this.sender = new HttpRequestSender(this.proxyHost, this.proxyPort);
    }

    public void setUserAgent() {
        this.headers.put("User-Agent", this.DEFAULT_USER_AGENT);
    }

    public HttpResponse get(String url) {
        return this.sender.send(this.getUrl(url), "GET", this.headers, "");
    }

    public HttpResponse head(String url) {
        return this.sender.send(this.getUrl(url), "HEAD", this.headers, "");
    }

    public HttpResponse post(String url, String body) {
        return this.sender.send(this.getUrl(url), "POST", this.headers, body);
    }

    public HttpResponse put(String url, String body) {
        return this.sender.send(this.getUrl(url), "PUT", this.headers, body);
    }

    public HttpResponse delete(String url) {
        return this.sender.send(this.getUrl(url), "DELETE", this.headers, "");
    }

    public HttpResponse connect(String url) {
        return this.sender.send(this.getUrl(url), "CONNECT", this.headers, "");
    }

    public HttpResponse options(String url) {
        return this.sender.send(this.getUrl(url), "OPTIONS", this.headers, "");
    }

    public HttpResponse trace(String url) {
        return this.sender.send(this.getUrl(url), "TRACE", this.headers, "");
    }

    public HttpResponse patch(String url, String body) {
        return this.sender.send(this.getUrl(url), "PATCH", this.headers, body);
    }

    private URL getUrl(String url) {
        URL thisUrl;
        try {
            thisUrl = new URL(url);
            return thisUrl;
        } catch (MalformedURLException var5) {
            try {
                thisUrl = new URL(this.baseUrl, url);
                return thisUrl;
            } catch (MalformedURLException var4) {
                var4.printStackTrace();
                System.out.println("What url are you trying to build? " + this.baseUrl.toString() + url);
                return null;
            }
        }
    }

    public void setHeader(String headername, String headervalue) {
        this.headers.put(headername, headervalue);
    }

    public void setBasicAuth(String username, String password) {
        String basicAuth = username + ":" + password;
        basicAuth = Base64.getEncoder().encodeToString(basicAuth.getBytes());
        this.setHeader("Authorization", "Basic " + basicAuth);
    }

    public void deleteHeader(String headername) {
        this.headers.remove(headername);
    }

    public String bodyToString(String fileName) {
        JsonParser parser = new JsonParser();
        try {
            Object object = parser.parse(new FileReader(fileName));
            JsonObject jsonObject = (JsonObject) object;

            return jsonObject.toString();

        } catch (FileNotFoundException e) {
            e.printStackTrace();

            return e.toString();
        }
    }
}
