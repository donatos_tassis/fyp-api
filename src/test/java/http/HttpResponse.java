package http;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class HttpResponse {
    public int statusCode;
    public String body;
    private Map<String, String> headers = new HashMap();

    public HttpResponse() {
    }

    public Map<String, String> getHeaders() {
        return this.headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers.putAll(headers);
    }

    public JsonObject asJson() {
        return new JsonParser().parse(this.body).getAsJsonObject();
    }

    public String raw() {
        StringBuilder rawOutput = new StringBuilder();
        Iterator var2 = this.headers.entrySet().iterator();

        Entry header;
        while(var2.hasNext()) {
            header = (Entry)var2.next();
            if (header.getKey() == null) {
                rawOutput.append((String)header.getValue());
                rawOutput.append("\n");
            }
        }

        var2 = this.headers.entrySet().iterator();

        while(var2.hasNext()) {
            header = (Entry)var2.next();
            if (header.getKey() != null && !((String)header.getKey()).equalsIgnoreCase("Transfer-Encoding")) {
                rawOutput.append((String)header.getKey());
                rawOutput.append(": ");
                rawOutput.append((String)header.getValue());
                rawOutput.append("\n");
            }
        }

        if (this.body != null) {
            rawOutput.append("\n");
            rawOutput.append(this.body);
        }

        return rawOutput.toString();
    }
}
