//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package http;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.Proxy.Type;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class HttpRequestSender {

    Proxy proxy;
    public HttpRequestDetails lastRequest;

    public HttpRequestSender(String proxyHost, int proxyPort) {
        if (proxyHost != null) {
            this.proxy = new Proxy(Type.HTTP, new InetSocketAddress(proxyHost, proxyPort));
        }

    }

    public HttpResponse send(URL url, String verb, Map<String, String> headers, String body) {
        HttpResponse response = new HttpResponse();

        try {
            HttpURLConnection con;
            if (this.proxy == null) {
                con = (HttpURLConnection)url.openConnection();
            } else {
                con = (HttpURLConnection)url.openConnection(this.proxy);
            }

            String var7 = verb.toLowerCase();
            byte var8 = -1;
            switch(var7.hashCode()) {
                case 106438728:
                    if (var7.equals("patch")) {
                        var8 = 0;
                    }
                    break;
                case 951351530:
                    if (var7.equals("connect")) {
                        var8 = 1;
                    }
            }

            switch(var8) {
                case 0:
                    headers.put("X-HTTP-Method-Override", "PATCH");
                    con.setRequestMethod("POST");
                    break;
                case 1:
                    headers.put("X-HTTP-Method-Override", "CONNECT");
                    con.setRequestMethod("POST");
                    break;
                default:
                    headers.remove("X-HTTP-Method-Override");
                    con.setRequestMethod(verb);
            }

            Iterator var15 = headers.keySet().iterator();

            while(var15.hasNext()) {
                String headerName = (String)var15.next();
                con.setRequestProperty(headerName, (String)headers.get(headerName));
                System.out.println("Header - " + headerName + " : " + (String)headers.get(headerName));
            }

            System.out.println("\nSending '" + verb + "' request to URL : " + url);
            if (body.length() > 0) {
                System.out.println(verb + " Body : " + body);
                con.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.writeBytes(body);
                wr.flush();
                wr.close();
            }

            int statusCode = con.getResponseCode();
            response.statusCode = statusCode;
            System.out.println("Response Code : " + statusCode);
            String responseBody = this.getResponseBody(con);
            System.out.println("Response Body: " + responseBody);
            response.body = responseBody.toString();
            Map<String, String> responseHeaders = new HashMap();
            Iterator var11 = con.getHeaderFields().keySet().iterator();

            String sentHeader;
            while(var11.hasNext()) {
                sentHeader = (String)var11.next();
                String headerValue = con.getHeaderField(sentHeader);
                responseHeaders.put(sentHeader, headerValue);
                System.out.println("Header: " + sentHeader + " - " + headerValue);
            }

            response.setHeaders(responseHeaders);
            this.lastRequest = new HttpRequestDetails();
            var11 = this.lastRequest.getHeaders().keySet().iterator();

            while(var11.hasNext()) {
                sentHeader = (String)var11.next();
                System.out.println(String.format("Request Header - %s:%s", sentHeader, this.lastRequest.getHeaders().get(sentHeader)));
            }

            if (body.length() > 0) {
                this.lastRequest.body = body;
            }

            return response;
        } catch (Exception var14) {
            var14.printStackTrace();
            throw new RuntimeException(var14);
        }
    }

    private String getResponseBody(HttpURLConnection con) {
        BufferedReader in = null;

        try {
            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        } catch (Exception var7) {
            InputStream stream = con.getErrorStream();
            if (stream != null) {
                in = new BufferedReader(new InputStreamReader(stream));
            }
        }

        StringBuffer responseBody = new StringBuffer();

        try {
            if (in != null) {
                String inputLine;
                while((inputLine = in.readLine()) != null) {
                    responseBody.append(inputLine);
                }

                in.close();
            }
        } catch (IOException var6) {
            var6.printStackTrace();
        }

        return responseBody.toString();
    }
}
