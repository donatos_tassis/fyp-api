package api.starter;

import spark.Spark;

public abstract class SparkStarter {

    protected int sparkPort = 4567;

    public SparkStarter() {}

    public abstract boolean isRunning();

    public abstract void startServer();

    public void startSparkAppIfNotRunning(int expectedPort) {
        this.sparkPort = expectedPort;

        try {
            System.out.println("Checking if running for integration tests");
            if (!this.isRunning()) {
                System.out.println("Not running - starting");
                this.startServer();
                System.out.println("Running spark to start");
            }
        } catch (IllegalStateException var4) {
            var4.printStackTrace();
            System.out.println("TODO: Investigate - " + var4.getMessage());
        }

        try {
            this.sparkPort = Spark.port();
        } catch (Exception var3) {
            System.out.println("Warning: could not get actual Spark port");
        }

        this.waitForServerToRun();
    }

    private void waitForServerToRun() {
        for(int tries = 10; tries > 0; --tries) {
            if (this.isRunning()) {
                return;
            }

            try {
                Thread.sleep(1000L);
            } catch (InterruptedException var3) {
                var3.printStackTrace();
            }
        }

        System.out.println("Warning: Server might not have started");
    }

    public void killServer() {
        Spark.stop();

        for(int tries = 10; tries > 0; --tries) {
            System.out.println("Checking if server has stopped");
            if (!this.isRunning()) {
                System.out.println("Server has stopped");
                return;
            }

            try {
                System.out.println("Spark threads " + Spark.activeThreadCount());
                Thread.sleep(1000L);
            } catch (InterruptedException var3) {
                var3.printStackTrace();
            }
        }

        System.out.println("Server might not have stopped");
    }

    public int getPort() {
        return this.sparkPort;
    }
}
