package api.starter;

import http.HttpMessageSender;
import http.HttpResponse;
import main.Main;
import spark.Spark;


public class SearchAPIStarter extends SparkStarter {

    private static SearchAPIStarter starter;
    private final String host;
    private final String path;

    // these should mainly test routing and SparkApiRequest/Response mapping
    static SearchAPIStarter server;

    private SearchAPIStarter(String host, String path) {
        this.host = host;
        this.path = path;
    }

    public static SearchAPIStarter get(String host) {
        if (starter == null) {
            starter = new SearchAPIStarter(host, "/test");
        }

        return starter;
    }

    public boolean isRunning() {
        try {
            HttpResponse response =
                    (new HttpMessageSender("http://" + this.host + ":" + Spark.port())).get(this.path);

            return response.statusCode == 200;
        } catch (Exception var2) {
            return false;
        }
    }

    public void startServer() {
        String [] args = {};
        Main.main(args);
        System.out.println("Run main to start");
    }
}
