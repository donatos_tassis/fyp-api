package models;

import org.junit.Test;

public class FiftyTablesTest extends AbstractSearchTest{


    private static final String FILENAME = "/search-samples/50-tables.json";

    private static final String PATH_SIMPLE = "/search-simple";
    private static final String PATH_STRICT = "/search-strict";
    private static final String PATH_HYBRID = "/search-hybrid";

    @Test
    public void searchSimpleTest() {
        super.templateTest(FILENAME, PATH_SIMPLE);
    }

    @Test
    public void searchStrictTest() {
        super.templateTest(FILENAME, PATH_STRICT);
    }

    @Test
    public void searchHybridTest() {
        super.templateTest(FILENAME, PATH_HYBRID);
    }
}
