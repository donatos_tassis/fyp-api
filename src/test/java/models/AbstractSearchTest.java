package models;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import http.HttpMessageSender;
import http.HttpResponse;
import jsonvalidator.retrieved.DataRetriever;
import jsonvalidator.retrieved.Party;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import spark.Spark;

import java.io.IOException;

import static api.starter.SearchAPIStarter.get;
import static helper.RandomGenerator.getRandomNumberInRange;

public class AbstractSearchTest {

    protected HttpMessageSender http;

    public AbstractSearchTest() {}

    @BeforeClass
    public static void startServer() {
        get("localhost").startSparkAppIfNotRunning(4567);
    }

    @Before
    public void httpConnect() {
        this.http = new HttpMessageSender("http://localhost:" + Spark.port());
    }

    // Warning -- all tests using this function takes considerable amount of time to be executed
    void templateTest(String filename, String path) {
        HttpMessageSender request = this.http;
        this.http.getClass();
        this.http.getClass();
        request.setHeader("Accept", "application/json");

        String fileName = getClass().getResource(filename).getFile();
        String body = http.bodyToString(fileName);

        ObjectMapper mapper = new ObjectMapper();
        HttpResponse response = this.http.post(path, body);
        JsonObject jsonObject = response.asJson();

        try {
            DataRetriever data = mapper.readValue(body, DataRetriever.class);
            if(!data.isValid()) {
                Assert.assertEquals(400, response.statusCode);
                Assert.assertTrue(response.asJson().isJsonObject());
                Assert.assertEquals(Integer.toString(data.getFailedRequests()), jsonObject.get("failedRequests").getAsString());
                Assert.assertEquals("Invalid data passed", jsonObject.get("error").getAsString());
            } else {
                Assert.assertEquals(200, response.statusCode);
                Assert.assertTrue(response.asJson().isJsonObject());

                HttpResponse newResponse;
                int id = 2;
                if(jsonObject.has("error")) {
                    data.removeLastParty();
                    data.setFailedRequests(data.getFailedRequests() + 1);
                    id--;
                }

                int emptyChairs = jsonObject.get("emptyChairs").getAsInt();
                int simultFails = 0;
                while(data.getCurrentCovers() < data.getTargetCovers()) {
                    Party party = new Party();
                    party.setId(id);
                    party.setSize(getRandomNumberInRange(1, data.getMaxTableCapacity()));
                    party.setStart(getRandomNumberInRange(1, data.getTimeSlots() - data.getDinnerDuration() + 1));
                    data.addParty(party);

                    String jsonString = mapper.writeValueAsString(data);
                    newResponse = this.http.post(path, jsonString);
                    JsonObject newJsonObject = newResponse.asJson();

                    if(newJsonObject.has("error")) {
                        simultFails++;
                        data.removeLastParty();
                        data.setFailedRequests(data.getFailedRequests() + 1);
                        Assert.assertEquals(404, newResponse.statusCode);
                        Assert.assertTrue(newResponse.asJson().isJsonObject());
                    } else {
                        simultFails = 0;
                        id++;
                        emptyChairs = newJsonObject.get("emptyChairs").getAsInt();
                        Assert.assertEquals(200, newResponse.statusCode);
                        Assert.assertTrue(newResponse.asJson().isJsonObject());
                    }

                    System.out.println("Current covers : " + data.getCurrentCovers());
                    if(simultFails > 10) {
                        break;
                    }
                }

                System.out.println("Total requests : " +
                        (data.getFailedRequests() + data.getParties().size()));
                System.out.println("Accepted requests : " + data.getParties().size());
                System.out.println("Rejected requests : " + data.getFailedRequests());
                System.out.println("Empty chairs : " + emptyChairs);
            }

        } catch (IOException e) {
            Assert.assertEquals(400, response.statusCode);
            Assert.assertTrue(response.asJson().isJsonObject());
            Assert.assertEquals("Invalid data passed", jsonObject.get("error").getAsString());
        }
    }
}
